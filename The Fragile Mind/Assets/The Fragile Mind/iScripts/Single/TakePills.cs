using UnityEngine;
using System.Collections;

public class TakePills: SingleIScript
{
	public string dialogue;
	
	GameObject textDisplay, camera;

	protected override void OnSuccess()
	{
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		camera = GameObject.FindGameObjectWithTag("MainCamera");
		
		textDisplay.GetComponent<GUITextDisplay>().getText(dialogue);
		
		camera.GetComponent<VFX>().Fade(true, 3, 2, "A2R6_BadEnd");
	}
	
	protected override void OnOutOfRange()
	{
	}
}

