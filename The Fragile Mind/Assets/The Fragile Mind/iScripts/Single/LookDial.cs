using UnityEngine;
using System.Collections;

public class LookDial : SingleIScript
{
	public string dialogue;

	GameObject textDisplay;
	
	protected override void OnSuccess()
	{
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");

		textDisplay.GetComponent<GUITextDisplay>().getText(dialogue);
	}
	
	protected override void OnOutOfRange()
	{
	}
}

