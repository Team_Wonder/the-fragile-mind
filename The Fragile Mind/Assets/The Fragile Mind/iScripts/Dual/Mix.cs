using UnityEngine;
using System.Collections.Generic;

public class Mix : DualIScript
{
	string dialogueG, dialogueB;
	
	GameObject textDisplay, camera;
	
	protected override void OnSuccess(Interactable other)
	{
		dialogueG = "#P'Ugh!....';" +
					"#T'This tastes like shit!'.;" +
					"#P'Argh..My head...';" +
					"...";
		
		dialogueB = "#P'Ugh....';" +
					"#T'I...I can't control...';" +
					"#F'!!!';" +
					"#Y'ARGH!!..';" +
					"...";
		
		camera = GameObject.FindGameObjectWithTag("MainCamera");
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		if (other.name == "CsOH" && this.gameObject.name == "O2F2") {
			textDisplay.GetComponent<GUITextDisplay>().getText(dialogueG);
			camera.GetComponent<VFX>().Fade(true, 3, 2, "A4R4_GoodEnd");
		} else if (other.name == "O2F2" && this.gameObject.name == "CsOH") {
			textDisplay.GetComponent<GUITextDisplay>().getText(dialogueG);
			camera.GetComponent<VFX>().Fade(true, 3, 2, "A4R4_GoodEnd");
		} else {
			textDisplay.GetComponent<GUITextDisplay>().getText(dialogueB);
			camera.GetComponent<VFX>().Fade(true, 3, 2, "A3R5_BadEnd");
		}
	}
	
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
