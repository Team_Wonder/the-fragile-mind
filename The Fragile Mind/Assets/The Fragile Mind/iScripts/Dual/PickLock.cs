using UnityEngine;
using System.Collections.Generic;

public class PickLock : DualIScript
{
	// This is only for use in A1R3
	protected override void OnSuccess(Interactable other)
	{
		if (!other.InInventory)
		{
			if (other.name == "Window") {
				Debug.Log("open window");
				AudioController.Play ("hindgeCreaking");
				// Insert level change here
				Application.LoadLevel ("A1R5_OutsideStreet");
			}
		}
	}
	
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
