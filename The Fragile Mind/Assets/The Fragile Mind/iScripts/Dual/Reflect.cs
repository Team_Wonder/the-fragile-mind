using UnityEngine;
using System.Collections.Generic;

public class Reflect : DualIScript
{
	[SerializeField] string dialogue;
	
	GameObject textDisplay, camera;
	
	protected override void OnSuccess(Interactable other)
	{

		camera = GameObject.FindGameObjectWithTag("MainCamera");
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		if (other.name == "Letter") {
			textDisplay.GetComponent<GUITextDisplay>().getText(dialogue);
			camera.GetComponent<VFX>().Fade(true, 3, 2, "A2R8_OutsideStreet");
		}
	}
	
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
