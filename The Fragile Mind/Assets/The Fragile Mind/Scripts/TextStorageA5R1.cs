﻿using UnityEngine;
using System.Collections;
using System.Reflection;

// This is the framework for all gameplay scenes
public class TextStorageA5R1 : MonoBehaviour {
	
	GameObject textDisplay, progression;
	
	public bool initialDial;
	
	string[] paragraph = new string[10];

	void Start () {
		// Set this to currrent scene ID
		string curClass = this.GetType().Name;
		Debug.Log (curClass.Length);
		string curScene = curClass.Substring(curClass.Length - 4, 4);
		
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		progression = GameObject.FindGameObjectWithTag("progression");
		text ();
		
		// Initial text for scene
		if (!progression.GetComponent<Progression>().getBeenTo(curScene)) {
			if (initialDial) {
				textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
			}
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(true);
			progression.GetComponent<Progression>().setBeenTo(curScene);
		} else {
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(false);
		}
	}
	
	void text () {
		/* Change text to be displayed in the scene here.
		 * 
		 * A paragraph maybe contain any text related to one object/interaction, and maybe
		 * split up into text blocks, that will display one by one, sepereate text blocks with ';',
		 * (do not add ; to the end of the paragraph, it will generate a new block with no content).
		 * 
		 * Each block of text has a 256 char limit, 5 lines (up to 4 '\n'), and 86 char per line.
		 * 
		 * Attach paragraphs to object/interaction by referencing the array index.
		 */
		paragraph[0] = 	";#NI have accepted what has happened to me..." +
						";With knowledge I will be able to look to the future.."+ 
				        ";#L...the past no longer has a hold on me"+
						";#LElizabeth, i am sure, has forgiven me\n\n and has gone to a better place.."+
						";#Li know look on everybody in this sad War with compassion"+
						";#L..re-building my life will be a first step, in creating a new world"+
						";#LI hope everyone will live without conflict,\n\n and with a happy mind...;"
				;
	}
	
	void Update () {
		
		/* example for passing text to GUI
		 * textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
		 */


		
	}
}
