﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class Progression : MonoBehaviour {
	
	public static Progression instance;
	
	Dictionary<string, bool> beenTo = new Dictionary<string, bool>();
	
	void Awake () {
		if (instance) {
			Destroy (gameObject);
		} else {
			Progression.instance = this;
			DontDestroyOnLoad(gameObject);
		}
		setScenes();
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	void setScenes() {
		// For each scene where scene loading is complete, add scene here
		// Do not add for scenes such as main menu
		beenTo.Add("A1R1", false);
		beenTo.Add("A1R2", false);
		beenTo.Add("A1R3", false);
		beenTo.Add("A1R4", false);
		beenTo.Add("A1R5", false);
		beenTo.Add("A2R1", false);
		beenTo.Add("A2R2", false);
		beenTo.Add("A2R3", false);
		beenTo.Add("A2R4", false);
		beenTo.Add("A2R5", false);
		beenTo.Add("A3R1", false);
		beenTo.Add("A3R2", false);
		beenTo.Add("A3R3", false);
		beenTo.Add("A3R4", false);
		beenTo.Add("A4R1", false);
		
	}
	
	// Sets the scene to visited
	public void setBeenTo (string s) {
		
		if (beenTo.ContainsKey(s)) {
			if (!beenTo[s]) {
				beenTo[s] = true;
			} else {
				Debug.LogError("Scene " + s + " has already been set as 'true'");	
			}
		} else {
			Debug.LogError("Scene " + s + " does not exist");	
		}
	}
	
	public bool getBeenTo (string s) {
		
		if (beenTo.ContainsKey(s)) {
			if (beenTo[s]) {
				return true;	
			} else {
				return false;	
			}
		} else {
			Debug.LogError("Scene " + s + " does not exist");
			return false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
