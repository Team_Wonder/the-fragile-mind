﻿using UnityEngine;
using System.Collections;

public class Reset : MonoBehaviour {
	
	GameObject toRemove;
	
	// Use this for initialization
	void Start () {
		toRemove = GameObject.Find("Player");
		Destroy (toRemove);
		toRemove = GameObject.Find ("Game");
		Destroy (toRemove);
		toRemove = GameObject.Find ("ProgressionTracker");
		Destroy (toRemove);
		toRemove = GameObject.Find("TextDisplay");
		Destroy (toRemove);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
