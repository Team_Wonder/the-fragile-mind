﻿using UnityEngine;
using System.Collections;

public class GUISceneChange : MonoBehaviour {
	
	public GUISkin baseGUISkin;
	public string levelName;
	public enum Arrow {Up, Down, Right, Left}
	public Arrow arrowDirection = Arrow.Up;
	
	GameObject textDisplay, camera;
	
	// Use this for initialization
	void Start () {
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		camera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI () {
		switch (arrowDirection) {
		case Arrow.Up :
			if (GUI.Button (new Rect (384, 10, 32, 32), "", baseGUISkin.customStyles[1])) {
				textDisplay.GetComponent<GUITextDisplay>().setSceneChange();
				camera.GetComponent<VFX>().Fade(true, 2, 0, levelName);
			}
			break;
		case Arrow.Down :
			if (GUI.Button (new Rect (384, 558, 32, 32), "", baseGUISkin.customStyles[0])) {
				textDisplay.GetComponent<GUITextDisplay>().setSceneChange();
				camera.GetComponent<VFX>().Fade(true, 2, 0, levelName);
			}
			break;
		case Arrow.Right :
			if (GUI.Button (new Rect (750, 288, 32, 32), "", baseGUISkin.customStyles[2])) {
				textDisplay.GetComponent<GUITextDisplay>().setSceneChange();
				camera.GetComponent<VFX>().Fade(true, 2, 0, levelName);	
			}
			break;
		case Arrow.Left :
			if (GUI.Button (new Rect (10, 288, 32, 32), "", baseGUISkin.customStyles[3])) {
				textDisplay.GetComponent<GUITextDisplay>().setSceneChange();
				camera.GetComponent<VFX>().Fade(true, 2, 0, levelName);	
			}
			break;
		default :
			Debug.Log ("Invalid arrow direction");
			break;
		}
	}
}
