﻿using UnityEngine;
using System.Collections;
using System.Reflection;

// This is the framework for all gameplay scenes
public class TextStorageA2R8 : MonoBehaviour {
	
	GameObject textDisplay, progression;
	
	public bool initialDial;
	
	string[] paragraph = new string[10];

	void Start () {
		// Set this to currrent scene ID
		string curClass = this.GetType().Name;
		Debug.Log (curClass.Length);
		string curScene = curClass.Substring(curClass.Length - 4, 4);
		
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		progression = GameObject.FindGameObjectWithTag("progression");
		text ();
		
		// Initial text for scene
		if (!progression.GetComponent<Progression>().getBeenTo(curScene)) {
			if (initialDial) {
				textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
			}
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(true);
			progression.GetComponent<Progression>().setBeenTo(curScene);
		} else {
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(false);
		}
	}
	
	void text () {
		/* Change text to be displayed in the scene here.
		 * 
		 * A paragraph maybe contain any text related to one object/interaction, and maybe
		 * split up into text blocks, that will display one by one, sepereate text blocks with ';',
		 * (do not add ; to the end of the paragraph, it will generate a new block with no content).
		 * 
		 * Each block of text has a 256 char limit, 5 lines (up to 4 '\n'), and 86 char per line.
		 * 
		 * Attach paragraphs to object/interaction by referencing the array index.
		 */
		paragraph[0] = 	";'Dear Issac,';" +
						"'- ---- - --- ---- -- - --- thinking, I just thought it -------- ----- --- best for us.';" +
						"'I've heard about the -------- ----- --- ---- on soliders back at home, I really hope you ---- ---- ---';" +
						"'--- ---- ---- me that old Ben ------- ----- --- ---- --- and that they haven't ------ -----, ----- ----- knows what happened';" +
						"'Elizabeth';#F'!!!';#N'Eli- Elizabeth...';#N'I...';" +
						"I have started to understand, but not just yet.;" +
						"I've only confirmed what I already  knew, but now I need to learn what I don't.;" +
						"#P'I don't feel too well out here...';" +
						"A particular smell was carried over by the wind and rain...;" +
						"As much as this doesn't make sense...;" + 
						"for me to say in this state of mind...;" +
						"#T...but it feels familiar...,;" +
						"#P...Too familiar perhaps.";
	}
	
	void Update () {
		
		/* example for passing text to GUI
		 * textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
		 */


		
	}
}
