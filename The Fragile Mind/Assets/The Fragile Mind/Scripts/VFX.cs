﻿using UnityEngine;
using System.Collections;

public class VFX : MonoBehaviour {
	
	float alpha, time, countTime;
	string changeLevel;
	bool isFade = false, countDown = false, fadeOut;
	[SerializeField] Texture black;
	
	// Use this for initialization
	void Start () {
		Fade (false, 2, 0, "0");
	}
	
	// Fade effect <isFadeOut> <fadeTime> <fadeCountDown> <sceneAfterFade>
	public void Fade (bool b, float t, float c, string s) {
		changeLevel = null;
		if (b) {
			fadeOut = true;
			alpha = 0;
			if (s != "0") {
				changeLevel = s;
			}
		} else {
			fadeOut = false;
			alpha = 1;	
		}
		time = t;
		if (c > 0) {
			countTime = c;
			countDown = true;
		} else {
			isFade = true;
		}
	}

	void OnGUI () {
		if (isFade) {
			if (fadeOut) {
				alpha += Mathf.Clamp01 (Time.deltaTime / time);
				if (alpha >= 1) {
					if (changeLevel != null) {
						Application.LoadLevel (changeLevel);
					}
				}
			} else {
				alpha -= Mathf.Clamp01 (Time.deltaTime / time);	
				if (alpha <= 0) {
					isFade = false;	
				}
			}
			
			GUI.color = new Color(0, 0, 0, alpha);
			GUI.DrawTexture (new Rect(0, 0, Screen.width, Screen.height), black);

		}
	}
	
	// Update is called once per frame
	void Update () {
		if (countDown) {
			print (countTime);
			if (countTime > 0) {
				countTime -= Time.deltaTime;	
			} else {
				isFade = true;
				countDown = false;
			}
		}
	}
}
