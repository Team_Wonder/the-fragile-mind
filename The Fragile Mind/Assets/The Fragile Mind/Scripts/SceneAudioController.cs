﻿using UnityEngine;
using System.Collections;

public class SceneAudioController : MonoBehaviour {
	
	public static SceneAudioController instance;
	
	void Awake () {
		if (instance) {
			Destroy (gameObject);
		} else {
			SceneAudioController.instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}
	
	void BGM () {
		// nightOfChaos
		if (Application.loadedLevelName == "A1R7_Epilogue" ||
			Application.loadedLevelName == "A2R7_Epilogue" ||
			Application.loadedLevelName == "A3R6_Epilogue" ||
			Application.loadedLevelName == "A3R7_Epilogue2" ||
			Application.loadedLevelName == "A4R7_Epilogue") {
			if (!AudioController.IsPlaying("nightOfChaos")) {
				AudioController.Play ("nightOfChaos");
			}
		} else {
			AudioController.Stop ("nightOfChaos");	
		}
		
		// blueFeather
		if (Application.loadedLevelName == "A0R0_MainMenu" || 
			Application.loadedLevelName == "A0R1_Prologue" ||
			Application.loadedLevelName == "A2R0_OutsideWifeHouse" ||
			Application.loadedLevelName == "A1R5_OutsideStreet") {
			if (!AudioController.IsPlaying("blueFeather")) {
				AudioController.Play ("blueFeather");
			}
		} else {
			AudioController.Stop ("blueFeather");	
		}
		
		// aftermath
		if (Application.loadedLevelName == "A3R1_HospitalFoyer" ||
			Application.loadedLevelName == "A3R2_HospitalSurgeryRoom" ||
			Application.loadedLevelName == "A3R3_HospitalWard" ||
			Application.loadedLevelName == "A3R4_HospitalMaintenance") {
			if (!AudioController.IsPlaying("aftermath")) {
				AudioController.Play ("aftermath");
			}
		} else {
			AudioController.Stop ("aftermath");	
		}
		
		// longTwoNote
		if (Application.loadedLevelName == "A1R1_BedRoom" || 
			Application.loadedLevelName == "A1R2_LivingRoom" ||
			Application.loadedLevelName == "A1R3_BathRoom" || 
			Application.loadedLevelName == "A1R4_Kitchen") {
			if (!AudioController.IsPlaying("longNoteTwo")) {
				AudioController.Play ("longNoteTwo");
			}
		} else {
			AudioController.Stop ("longNoteTwo");	
		}
		
		// sunset
		if (Application.loadedLevelName == "A4R1_TheBar") {
			if (!AudioController.IsPlaying("sunsetAtGlengorm")) {
				AudioController.Play ("sunsetAtGlengorm");
			}
		} else {
			AudioController.Stop ("sunsetAtGlengorm");	
		}
		
		// fourTemptress
		if (Application.loadedLevelName == "A2R1_WifeLivingRoom" ||
			Application.loadedLevelName == "A2R2_WifeBedRoom" ||
			Application.loadedLevelName == "A2R3_WifeBathRoom" ||
			Application.loadedLevelName == "A2R4_WifeKitchen" ||
			Application.loadedLevelName == "A2R5_WifeBasement") {
			if (!AudioController.IsPlaying("fourTemptress")) {
				AudioController.Play ("fourTemptress");
			}
		} else {
			AudioController.Stop ("fourTemptress");	
		}
		
		//plaint
		if (Application.loadedLevelName == "A4R4_GoodEnd") {
			if (!AudioController.IsPlaying("plaint")) {
				AudioController.Play ("plaint");
			}
		} else {
			AudioController.Stop ("plaint");	
		}
		
		//sovereign
		if (Application.loadedLevelName == "A4R5_EpilogueFinal") {
			if (!AudioController.IsPlaying("sovereign")) {
				AudioController.Play ("sovereign");
			}
		} else {
			AudioController.Stop ("sovereign");	
		}
	}
	
	void Effects () {
		// inside rain
		if (Application.loadedLevelName == "A1R1_BedRoom" ||
			Application.loadedLevelName == "A1R4_Kitchen") {
			if (!AudioController.IsPlaying("rainDrops")) {
				AudioController.Play("rainDrops");	
			}
		} else {
			AudioController.Stop ("rainDrops");	
		}
		
		// fire place
		if (Application.loadedLevelName == "A1R2_LivingRoom") {
			if (!AudioController.IsPlaying("fireplace")) {
				AudioController.Play("fireplace");	
			}
		} else {
			AudioController.Stop ("fireplace");	
		}
		
		// distant thunder and rain
		if (Application.loadedLevelName == "A1R3_BathRoom") {
			if (!AudioController.IsPlaying("distantThunderRain")) {
				AudioController.Play("distantThunderRain");	
			}
		} else {
			AudioController.Stop ("distantThunderRain");	
		}
		
		// outside rain
		if (Application.loadedLevelName == "A1R5_OutsideStreet" ||
			Application.loadedLevelName == "A2R0_OutsideWifeHouse" ||
			Application.loadedLevelName == "A2R8_OutsideStreet") {
			if (!AudioController.IsPlaying("outsideRain")) {
				AudioController.Play("outsideRain");	
			}
		} else {
			AudioController.Stop ("outsideRain");	
		}
		
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		BGM();
		Effects();
		
		
	}
}
