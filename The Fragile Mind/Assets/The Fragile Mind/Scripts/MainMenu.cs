﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	[SerializeField] Texture backgroundTexture;
	bool press1 = false;
	
	void Start() {
		animation["CameraAnim1"].wrapMode = WrapMode.Once;	
	}
	
	void OnGUI(){
	
		//display background texture
		//GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),backgroundTexture);	
		
		
		//displays Gui main Menu buttons
		if (GUI.Button (new Rect(Screen.width * .25f, Screen.height * .5f, Screen.width * .5f, Screen.height * .1f), "Play Game")){
			print ("Clicked Play game");
			animation.Play("CameraAnim1");
			press1 = true;
		}
		if (GUI.Button (new Rect(Screen.width * .25f, Screen.height * .8f, Screen.width * .5f, Screen.height * .1f), "Exit")){
			print ("Clicked Exit");
			Application.Quit();
		}
		if (GUI.Button (new Rect(Screen.width * .25f, Screen.height * .6f, Screen.width * .5f, Screen.height * .1f), "Credits")){
			print ("Clicked Credits");
			Application.LoadLevel("A0R3_Credits");
		}	
		if (GUI.Button (new Rect(Screen.width * .25f, Screen.height * .7f, Screen.width * .5f, Screen.height * .1f), "Instructions")){
			print ("Clicked Instructions");
			Application.LoadLevel("A0R2_Instructions");
		}	
	}
	
	void Update() {
		if (press1) {
			if (!animation.IsPlaying("CameraAnim1")) {
				Application.LoadLevel("A0R1_Prologue");	
			}
		}
	}
	
}
