﻿using UnityEngine;
using System.Collections;
using System.Reflection;

// This is the framework for all gameplay scenes
public class TextStorageA0R1 : MonoBehaviour {
	
	GameObject textDisplay, progression;
	
	public bool initialDial;
	
	string[] paragraph = new string[10];

	void Start () {
		// Set this to currrent scene ID
		string curClass = this.GetType().Name;
		Debug.Log (curClass.Length);
		string curScene = curClass.Substring(curClass.Length - 4, 4);
		
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		progression = GameObject.FindGameObjectWithTag("progression");
		text ();
		
		// Initial text for scene
		if (!progression.GetComponent<Progression>().getBeenTo(curScene)) {
			if (initialDial) {
				textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
			}
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(true);
			progression.GetComponent<Progression>().setBeenTo(curScene);
		} else {
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(false);
		}
	}
	
	void text () {
		/* Change text to be displayed in the scene here.
		 * 
		 * A paragraph maybe contain any text related to one object/interaction, and maybe
		 * split up into text blocks, that will display one by one, sepereate text blocks with ';',
		 * (do not add ; to the end of the paragraph, it will generate a new block with no content).
		 * 
		 * Each block of text has a 256 char limit, 5 lines (up to 4 '\n'), and 86 char per line.
		 * 
		 * Attach paragraphs to object/interaction by referencing the array index.
		 */
		paragraph[0] = 	";\"Amnesia... is a deficit in memory...\";" +
						"\"...Amnesia may be caused by psychological " +
						"trauma, or as a result of medical procedures...\";" +
						"\"...Amnesia can also be caused by the use of " +
						"sedatives or hypnotic drugs...\" - Wikipedia;" +
						"(Press the arrow below to proceed)";
	}
	
	void Update () {
		
		/* example for passing text to GUI
		 * textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
		 */


		
	}
}
