﻿using UnityEngine;
using System.Collections;
using System.Reflection;

// This is the framework for all gameplay scenes
public class TextStorageA1R1 : MonoBehaviour {
	
	GameObject textDisplay, progression;

	public bool initialDial;
	
	string[] paragraph = new string[1];

	void Start () {
		// Set this to currrent scene ID
		string curClass = this.GetType().Name;
		Debug.Log (curClass.Length);
		string curScene = curClass.Substring(curClass.Length - 4, 4);
		
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		progression = GameObject.FindGameObjectWithTag("progression");
		text ();
		
		// Initial text for scene
		if (!progression.GetComponent<Progression>().getBeenTo(curScene)) {
			if (initialDial) {
				textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
			}
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(true);
			progression.GetComponent<Progression>().setBeenTo(curScene);
		} else {
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(false);
		}
	}

	void text () {
		/* Change text to be displayed in the scene here.
		 * 
		 * A paragraph maybe contain any text related to one object/interaction, and maybe
		 * split up into text blocks, that will display one by one, sepereate text blocks with ';',
		 * (do not add ; to the end of the paragraph, it will generate a new block with no content).
		 * 
		 * Each block of text has a 256 char limit, 5 lines (up to 4 '\n'), and 86 char per line.
		 * 
		 * Attach paragraphs to object/interaction by referencing the array index.
		 */
		paragraph[0] = 	";. . .;" +
						"A strong musky smell enters my nose... ;" +
						"...the blurry image slowly forms around me...\n\n..soft grey light..\n\n..shadows.... ;" +
						"Shadows blur as i try to move... ;" +
						"A stabbing pain attacks my head... ;" +
						"#T'what.. am..I.. doing here'?;" +
						"The surrounding's startle me for a moment, but I soon realize I am lying in bed... ;" +
						"#T'Wait...why-';" +
						"the scene forms into a familiar one...\n\n My bedroom....;" +
						"#N'Why am I here?\n\n I haven't been here for years...' ;" +
						"The last thing I remember was...\n" +
						"\n being releasing from duty! ;" +
						"#N'But how did I end up here?'...;" +
						"I am in my own house, but I not have any memory of coming back here?;" +
						"#P'Maybe...;" +
						"#P'Maybe I had few too many drinks??.....';" +
						"#P'...Who am I kidding...;" +
						"Dazed..Confused, and feeling incredible pain, I get up, and begin to look around... ";
	}
	
	void Update () {
		
		/* example for passing text to GUI
		 * textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
		 */


		
	}
}
