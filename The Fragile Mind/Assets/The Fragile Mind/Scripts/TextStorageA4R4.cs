﻿using UnityEngine;
using System.Collections;
using System.Reflection;

// This is the framework for all gameplay scenes
public class TextStorageA4R4 : MonoBehaviour {
	
	GameObject textDisplay, progression;
	
	public bool initialDial;
	
	string[] paragraph = new string[10];

	void Start () {
		// Set this to currrent scene ID
		string curClass = this.GetType().Name;
		Debug.Log (curClass.Length);
		string curScene = curClass.Substring(curClass.Length - 4, 4);
		
		textDisplay = GameObject.FindGameObjectWithTag("textDisplay");
		progression = GameObject.FindGameObjectWithTag("progression");
		text ();
		
		// Initial text for scene
		if (!progression.GetComponent<Progression>().getBeenTo(curScene)) {
			if (initialDial) {
				textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
			}
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(true);
			progression.GetComponent<Progression>().setBeenTo(curScene);
		} else {
			textDisplay.GetComponent<GUITextDisplay>().setFirstRun(false);
		}
	}
	
	void text () {
		/* Change text to be displayed in the scene here.
		 * 
		 * A paragraph maybe contain any text related to one object/interaction, and maybe
		 * split up into text blocks, that will display one by one, sepereate text blocks with ';',
		 * (do not add ; to the end of the paragraph, it will generate a new block with no content).
		 * 
		 * Each block of text has a 256 char limit, 5 lines (up to 4 '\n'), and 86 char per line.
		 * 
		 * Attach paragraphs to object/interaction by referencing the array index.
		 */
		paragraph[0] = 	";...melancholy...;" +
						"#NI have managed to accept, all that has befallen me...;" +
						"#N...What others have done to me;" +
						"#P.....;" +
						"#NWhat I have done to others...;" +
						"#T.....;" +
						"#NIn my heart i know Elizabeth has forgiven me...;" +
						"#L...she has gone to a better place;" +
						"#LI think I will be eventually,be able to forgive what others have done to me...;" +
						"#LA new Dawn has appeared;" +
						"#L....possibilty;" +
						"#L........seems;" +
						"#L.............endless!;";
	}
	
	void Update () {
		
		/* example for passing text to GUI
		 * textDisplay.GetComponent<GUITextDisplay>().getText(paragraph[0]);
		 */


		
	}
}
