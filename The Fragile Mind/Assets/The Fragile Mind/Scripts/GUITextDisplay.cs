﻿using UnityEngine;
using System.Collections;

// This is the framework for
public class GUITextDisplay : MonoBehaviour {
    
	public static GUITextDisplay instance;
    public GUISkin baseGUISkin;
	
	[SerializeField] Texture[] experssions;
    bool textDisplayOn;
    string[] sentence;
	string level;
    int sentenceCount, i;
    bool firstRun;
	Component storage;
	
	void Awake () {
		if (instance) {
			Destroy (gameObject);
		} else {
			GUITextDisplay.instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}	

    void Start () {
        textDisplayOn = false;  
    }
	
    // Receives what text to display
    public void getText (string s) {
        sentence = s.Split(';');
        sentenceCount = -1;
		textDisplayOn = true;
		input ();
    }
	
	// Setter for first visit of a scene
	public void setFirstRun (bool b) {
		if (b) {
			firstRun = true;	
		} else {
			firstRun = false;	
		}
	}
	
	// Setter for hiding text display on scene change
	public void setSceneChange () {
		textDisplayOn = false;	
		sentence = null;
	}
    
	
	
    void input () {
		if (sentence != null) {
            textDisplayOn = true;
            if (sentenceCount < sentence.Length - 1) {
                sentenceCount++;        
            } else {
                sentenceCount = -1;
                textDisplayOn = false;
				sentence = null;
            }
		}
    }
    
    // Scroll through text
    void Update () {
		if (firstRun) {
			input ();
			firstRun = false;
		}
		if (Input.GetKeyUp(KeyCode.Space)) {
            input ();
        }
    }
            
    // Draw text box and text
    void OnGUI () {
        if (textDisplayOn) {
            	string _temp = sentence[sentenceCount];
				// <#F, #N, #P, #S, #Y, #T, #L>
				if (_temp[0] == '#') {
					switch (_temp[1]) {
					case 'F':
						i = 0;
						break;
					case 'N':
						i = 1;
						break;
					case 'P':
						i = 2;
						break;
					case 'S':
						i = 3;
						break;
					case 'Y':
						i = 4;
						break;
					case 'T':
						i = 5;
						break;
					case 'L':
						i = 6;
						break;
					default:
						Debug.LogError ("WARNING: invalid experssion paramter");
					break;
					}
				level = Application.loadedLevelName;
				if (level.Substring(1,1) == "2") {i+=7;}
				else if (level.Substring(1,1) == "3") {i+=14;}
				else if (level.Substring(1,1) == "4") {i+=21;}
					GUI.DrawTexture (new Rect (50, 410, 120, 120), experssions[i]);
					_temp = _temp.Substring(2,_temp.Length - 2);
	            	if (GUI.Button (new Rect (175, 410, 450, 120), _temp, baseGUISkin.label )) {input ();}
				} else {
            	if (GUI.Button (new Rect (175, 410, 450, 120), _temp, baseGUISkin.label )) {input ();}
				}
			} 
			
        
    }
}