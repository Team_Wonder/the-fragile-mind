using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Adventure/Player/Inventory")]
/// <summary>
/// The player's inventory.
/// </summary>
public class Inventory : MonoBehaviour
{
	#region Constants
	
	
	static readonly int AUTO_PADDING = 4;
	static readonly int LINE_HEIGHT = 22;
	static readonly int AVG_CHAR_WIDTH = 9;
	
	
	#endregion
	
	
	#region Inspector variables
	
	
	public int height;
	public int slotWidth;
	public int buttonWidth;
	public int emptyWidth;
	public int buttonSlotsPadding;
	public int xOffset;
	public int yOffset;
	public int menuPadding;
	
	
	#endregion
	
	
	#region Fields
	
	
	bool isShowing = false;
	
	string buttonLabel;
	Rect buttonArea;
	Rect slotsArea;
	
	Rect interactableMenuButtonArea;
	Rect interactableMenuTitleArea;
	Rect interactableMenuBorderArea;
	
	public bool InInteractableMenu
	{
		get { return selected != null; }
	}
	Interactable selected = null;
	
	List<Interactable> contents = new List<Interactable>();
	
	
	#endregion
	
	
	#region Unity event handlers
	
	
	void Start()
	{		
		buttonLabel = GetUpdatedButtonLabel();
		UpdateMenuArea();
		UpdateSelectedMenuArea();
	}
	
	/// <summary>
	/// Draws inventory toggle button and inventory slots.
	/// </summary>
	void OnGUI()
	{
		if (Game.Instance != null)
			GUI.skin = Game.Instance.CustomSkin;
		
		// an interactable's scene menu
		if (Player.Instance.InInteractableSceneMenu)
			GUI.enabled = false;
		
		// an interactable's inventory menu
		if (InInteractableMenu)
			GUI.enabled = false;
		
		if (GUI.Button(buttonArea, buttonLabel + " (" + Size() + ")"))
			Toggle();
		
		if (!isShowing)
			return;
		
		if (contents.Count > 0)
			DrawSlots();
		else
			DrawEmptySlots();
		
		if (InInteractableMenu)
		{
			if (!GUI.enabled)
				GUI.enabled = true;
			
			if (selected != null)
			{
				Vector2 slotStartPoint = GetSlotStartPoint(selected);
				
				bool pressed = selected.DrawInventoryMenuGUI(new Vector2(
					slotStartPoint.x, slotStartPoint.y - menuPadding));
				
				if (pressed)
				{
					TurnOffInteractableMenu();
				}
			}
		}
	}

	
	#endregion
	
	
	#region GUI helpers
	
	
	void DrawSlots()
	{
		bool pressed = false;
		Interactable hovering = null;
		
		GUILayout.BeginArea(slotsArea);
		
		GUILayout.BeginHorizontal();
		
		List<Interactable> contentsCopy = new List<Interactable>(contents);
		
		foreach (Interactable i in contentsCopy)
		{
			// only enable button toggle for interactable with inventory menu open
			if (InInteractableMenu && (i == selected))
				GUI.enabled = true;
			
			// only enable button toggle if selecting "other" interactable for dual interaction
			if (Player.Instance.InDualInteraction)
				GUI.enabled = true;
			
			pressed = GUILayout.Button(i.icon,
				GUILayout.Width(slotWidth), 
				GUILayout.Height(height));
			
			if (InInteractableMenu && (i == selected))
				GUI.enabled = false;
			
			if (Player.Instance.InDualInteraction)
				GUI.enabled = false;
	
			if (pressed)
			{
				if (Player.Instance.InDualInteraction)
				{
					// cancel dual interaction, pressed itself
					if (i == selected)
					{
						Player.Instance.SwitchFromDualInteractionMode();
						TurnOffInteractableMenu();
					}
					else
						Player.Instance.ExecuteDualInteraction(i);
				}
				else
				{
					// already selected
					if (i == selected)
						TurnOffInteractableMenu();
					else
						TurnOnInteractableMenu(i);
				}
			}
			// just hovering over slot
			else if (GetSlotArea(i).Contains(AdventureUtility.InvertedMouseY))
			{
				if (InInteractableMenu)
				{
					// select 'other' object in inventory while performing dual interaction
					if (Player.Instance.InDualInteraction)
						Player.Instance.SelectedInteractable = i;
				}
				else
				{
					hovering = i;
				}
			}
			else
			{
				if (InInteractableMenu)
				{
					// deselect 'other' object in inventory while performing dual interaction
					if (Player.Instance.InDualInteraction &&
						(Player.Instance.SelectedInteractable == i))
					{
						Player.Instance.SelectedInteractable = null;
					}
				}
			}
		}
		
		GUILayout.EndHorizontal();
		
		GUILayout.EndArea();
		
		if (hovering != null)
			DrawDescriptionBox(hovering);
	}
	
	void DrawEmptySlots()
	{
		GUILayout.BeginArea(slotsArea);
		
		GUILayout.Box("Empty",
			GUILayout.MinWidth(slotsArea.width),
			GUILayout.MinHeight(slotsArea.height));
		
		GUILayout.EndArea();
	}
	
	void DrawDescriptionBox(Interactable interactable)
	{
		if (interactable == null)
			return;
		
		Vector2 topLeftOfSlot = GetSlotStartPoint(interactable);
		
		GUI.Box(new Rect(
			topLeftOfSlot.x,
			topLeftOfSlot.y - menuPadding - LINE_HEIGHT,
			(interactable.Description.Length * AVG_CHAR_WIDTH) + (menuPadding * 4),
			LINE_HEIGHT + menuPadding),
			interactable.Description);
	}
	
	
	#endregion
	
	
	#region Contents helpers

	/// <summary>
	/// Gets number of interactables currently in player's inventory.
	/// </summary>
	public int Size()
	{
		return contents.Count;
	}
	
	/// <summary>
	/// Whether an interactable with a particular name is in player's inventory.
	/// </summary>
	public bool ContainsObject(string objectName)
	{
		return contents.Exists(i => (i.name == objectName));
	}
	
	/// <summary>
	/// Number of interactables with a particular name in player's inventory.
	/// </summary>
	public int GetObjectCount(string objectName)
	{
		return GetAllObjects(objectName).Count;
	}
	
	/// <summary>
	/// Gets reference to first interactable from player's inventory with a particular name,
	/// returns null if no object with that name is in the inventory.
	/// </summary>
	public Interactable GetObject(string objectName)
	{
		return contents.Find(i => (i.name == objectName));
	}
	
	/// <summary>
	/// Gets references to all interactable from player's inventory with a particular name,
	/// returns empty list if no objects with that name is in the inventory.
	/// </summary>
	public List<Interactable> GetAllObjects(string objectName)
	{
		return contents.FindAll(i => (i.name == objectName));
	}
	
	/// <summary>
	/// Add a new interactable to player's inventory.
	/// </summary>
	public void AddInteractables(List<Interactable> interactables)
	{
		if (interactables == null)
			return;
		
		foreach (Interactable i in interactables)
			AddInteractable(i);
	}
	
	/// <summary>
	/// Add a new interactable to player's inventory.
	/// </summary>
	public void AddInteractable(Interactable interactable)
	{
		if (interactable == null)
			return;
		
		contents.Insert(0, interactable);
		interactable.SetFromSceneToInventory();
		
		UpdateMenuArea();
		Helpers.UpdateNavMesh();
	}
	
	/// <summary>
	/// Removes interactable from player's inventory and place it another 
	/// gameObject's position.
	/// </summary>
	public void RemoveInteractable(Interactable interactable, GameObject go)
	{
		if (interactable == null)
			return;
		
		if (go == null)
			return;
		
		contents.Remove(interactable);
		interactable.SetFromInventoryToScene(go);
		
		UpdateMenuArea();
		Helpers.UpdateNavMesh();
	}
	
	/// <summary>
	/// Removes interactable from player's inventory and place it a point
	/// in the scene.
	/// </summary>
	public void RemoveInteractable(Interactable interactable, Vector3 point)
	{
		if (interactable == null)
			return;
		
		contents.Remove(interactable);
		interactable.SetFromInventoryToScene(point);
		
		UpdateMenuArea();
	}
	
	
	#endregion
	
	
	#region Public methods
	
	
	/// <summary>
	/// Whether a point on screen is inside the inventory,
	/// assuming (0, 0) is top-left of screen.
	/// </summary>
	/// <remarks>
	/// Useful for knowing whether player is clicking on inventory.
	/// </remarks>
	public bool IsScreenPointInside(Vector2 screenPoint)
	{
		if (buttonArea.Contains(screenPoint))
			return true;
		
		if (isShowing)
		{
			if (slotsArea.Contains(screenPoint))
				return true;
		}
		
		return false;
	}
	
	/// <summary>
	/// Hide / show the player's inventory.
	/// </summary>
	public bool Toggle()
	{
		isShowing = !isShowing;
		buttonLabel = GetUpdatedButtonLabel();
		
		return isShowing;
	}
	
	
	#endregion
	
	
	#region Updating
	
	
	/// <summary>
	/// Gets the updated main inventory button label.
	/// </summary>
	string GetUpdatedButtonLabel()
	{
		string label = isShowing ? "Close" : "Open";
		label += " Inventory";
		
		return label;
	}
	
	/// <summary>
	/// Updates the main button / slots area.
	/// </summary>
	void UpdateMenuArea()
	{
		buttonArea = new Rect(
			Screen.width - buttonWidth - xOffset,
			Screen.height - height - yOffset,
			buttonWidth, height);
		
		int totalSlotWidth = (slotWidth + AUTO_PADDING) * contents.Count;
		
		if (totalSlotWidth <= 0)
			totalSlotWidth = emptyWidth;
		
		slotsArea = new Rect(
			buttonArea.x - totalSlotWidth - buttonSlotsPadding,
			buttonArea.y, totalSlotWidth, buttonArea.height);
	}
	
	/// <summary>
	/// Updates the currently selected interactable's inventory menu's drawing area.
	/// </summary>
	void UpdateSelectedMenuArea()
	{
		if (selected == null)
			return;
		
		Vector2 topLeftOfSlot = GetSlotStartPoint(selected);
		selected.UpdateInventoryMenu(new Vector2(
			topLeftOfSlot.x, topLeftOfSlot.y - menuPadding));
	}
	
	
	#endregion
	
	
	#region Slot helpers
	
	
	/// <summary>
	/// Gets area on screen a slot is drawn.
	/// </summary>
	Rect GetSlotArea(int slotIndex)
	{
		if (slotIndex < 0)
			slotIndex = 0;
		
		Vector2 slotStartPoint = GetSlotStartPoint(slotIndex);
		
		return new Rect(slotStartPoint.x, slotStartPoint.y,
			slotWidth - 1, height - 1);
	}
	
	/// <summary>
	/// Gets area on screen a slot is drawn.
	/// </summary>
	Rect GetSlotArea(Interactable interactable)
	{
		int slotIndex = contents.IndexOf(interactable);
		
		if (slotIndex < 0)
			slotIndex = 0;
		
		return GetSlotArea(slotIndex);
	}
	
	/// <summary>
	/// Gets the top left point on screen a slot is drawn.
	/// </summary>
	Vector2 GetSlotStartPoint(Interactable interactable)
	{
		int slotIndex = contents.IndexOf(interactable);
		
		if (slotIndex < 0)
			slotIndex = 0;
		
		return GetSlotStartPoint(slotIndex);
	}
	
	/// <summary>
	/// Gets the top left point on screen a slot is drawn.
	/// </summary>
	Vector2 GetSlotStartPoint(int slotIndex)
	{
		if (slotIndex <= 0)
			slotIndex = 0;
		
		return new Vector2(
			slotsArea.x + (slotWidth + AUTO_PADDING) * slotIndex,
			slotsArea.y + 1);
	}
	
	
	#endregion
	
	
	#region Menu switching
	
	
	public void TurnOffInteractableMenu()
	{
		selected = null;
	}
	
	void TurnOnInteractableMenu(Interactable i)
	{
		if (i == null)
			return;
		
		selected = i;
		UpdateSelectedMenuArea();
	}
	
	
	#endregion
}
