using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Base script for all scripts can be added to an interactable (called an "iScript").
/// </summary>
public abstract class InteractionScript : MonoBehaviour
{
	/// <summary>
	/// Convenient hook for setting initial value of shared parameters (experimental feature).
	/// </summary>
	/// <remarks>
	/// I can't remember why I had to disable drag and drop in the inspector,
	/// feel free to re-enable and test it out.
	/// 
	/// Drag and drop in inspector was disabled for static parameters, forcing
	/// the setting of values to be done programatically in this function or
	/// one like it. A side-effect of this is parameters of some asset types
	/// (e.g., textures) can't be set in this function. Doing this is still
	/// possible by setting a shared parameter to an instance parameter value
	/// in Start(). A big drawback of doing this is the instance parameter's
	/// initial value can no longer be viewed in the editor / inspector.
	/// 
	/// Initialising shared parameters with instance parameter values in this
	/// function or one like it doesn't work. Setting shared parameters with
	/// instance parameter values can be done just fine in other functions,
	/// shared parameter will revert to initial value set in this function
	/// once game stops.
	/// </remarks>
	public virtual void InitSharedParameters(){}
	
	/// <summary>
	/// What to do when player wants to execute this interaction script.
	/// </summary>
	public virtual void OnExecute(string verbName)
	{
		if (interactable.InInventory)
		{
			// execute this script instantly
			OnInteract(true);
			Player.Instance.Inventory.TurnOffInteractableMenu();
		}
		else
		{
			// move before attempting to interact
			Player.Instance.Movement.MoveAndSingleInteract(transform.position, this);
		}
	}
	
	/// <summary>
	/// What to do when this interaction script actually starts.
	/// </summary>
	public abstract void OnInteract(bool success);
	
	/// <summary>
	/// The interactable this iScript is attached to.
	/// </summary>
	[SerializeField,HideInInspector]
	Interactable interactable;
	public Interactable Interactable
	{
		get { return interactable; }
		set
		{
			if (value == null)
				return;
			
			interactable = value;
		}
	}
	
	/// <remarks>
	/// Unity calls constructors of MonoBehaviour's automatically 
	/// (highly recommended not to call it yourself).
	/// </remarks>
	protected InteractionScript()
	{
		InitSharedParameters();
	}
	
}

