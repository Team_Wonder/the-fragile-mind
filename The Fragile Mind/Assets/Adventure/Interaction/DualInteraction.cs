using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// An interaction that can be made between two objects using a verb (e.g., combining).
/// </summary>
/// <remarks>
/// Make sure every field that you want saved is marked with [SerializeField]
/// (i.e., the variable saves its value when a game is started or when opening/closing the editor)
/// </remarks>
[Serializable()]
public class DualInteraction : Interaction
{
	#region Type
	
	
	[Flags]
	/// <summary>
	/// List of possible verbs.
	/// </summary>
	/// <remarks>
	/// Can be added to / removed at will, just make sure each has a
	/// unique power-of-two value: http://en.wikipedia.org/wiki/Bit_field)
	/// </remarks>
	public enum VerbType
	{
		UseWith = 1,
		PointAt = 2,
		CastFire = 4,
		PickLock = 8,
		Mix = 16,
		Reflect = 32,
	}
	
	[SerializeField,HideInInspector]
	VerbType verb;
	public VerbType Verb { get { return verb; } }
	public override string VerbAsString { get { return verb.ToString(); } }
	
	
	#endregion
	
	
	#region Initialisation
	
	
	public DualInteraction(GameObject go, VerbType verb) 
		: base(go, typeof(DualIScript), false, true)
	{		
		this.verb = verb;
	}
	
	
	#endregion
}