using UnityEngine;
using System.Collections.Generic;

public class CombineValve : DualIScript
{
	public string targetName;
	
	protected override void OnSuccess(Interactable other)
	{
		if (other.name == "Valve Stem")
		{
			GameObject target = Helpers.GetGameObject(targetName);
			Helpers.Inventory.RemoveInteractable(Interactable, target);
			Helpers.UpdateNavMesh();
		}
	}
	
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
