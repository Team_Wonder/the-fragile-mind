using UnityEngine;
using System.Collections;

public class Dialogue : MonoBehaviour {
	
	public Vector3 defaultOffset = new Vector3(-3.0f, 1.5f, 1.0f); //offset of the screen
	private GameObject dialogueContainer;
	public GameObject containerPrefab;
	public float speed = 0.06f; //seconds for each character
	public float midPause = 1.0f;
	public float endPause = 0.0f;
	public string[] dialogueArray;
	public bool isThoughtBubble = false;
	
	private bool isActive = false;
	private bool canBeClosed = false;
	private bool isWaiting = false;
	private float checkFrequency = 0.2f;

	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.E)){
			if (canBeClosed){
				Destroy(dialogueContainer);
				canBeClosed = false;
				isActive = false;
			}
			if (isWaiting){
				isWaiting = false;	
			}
		}
	}
	
	void DisplayDialogue(){
		if (!isActive){
			isActive = true;
			StartCoroutine(ReadDialogue());
		}
	}
	
	IEnumerator ReadDialogue(){
		//create a container for the text
		dialogueContainer = drawDialogueContainer();
		//cycle through each element of dialogue and treat that as a new break
		for (int j = 0; j < dialogueArray.Length; j++){
			string dialogue = dialogueArray[j].Replace("\\n", "\n"); //fix the \n's *sigh*
			TextMesh textMesh = dialogueContainer.GetComponentInChildren<TextMesh>();
			string printText = ""; 
			printText += dialogue[0];
	
			//first
			textMesh.text = printText;
			yield return new WaitForSeconds(speed);
			
			for (int i = 1; i < dialogue.Length; i++){
				//if there's a special character (for pausing, stopping, etc.)
				//for some reason, a # directly after a newline will not be read
				//THE SOLUTION is to put a space before the #
				if (dialogue[i] == '#'){
				Debug.Log("registering a hash: "+dialogue[i] +"" + dialogue[i+1]);
					if (dialogue[i+1] == 'p'){
						yield return new WaitForSeconds(midPause);
					}
					else if (dialogue[i+1] == 'w'){
						isWaiting = true;
						while (isWaiting){
							yield return new WaitForSeconds(checkFrequency);
						}
					}
					else if (dialogue[i+1] == 's'){
						gameObject.SendMessage("PlayEmotion", "shock");
						Debug.Log("playing shock");
					}
					i++;
				}
				//otherwise continue with normal operations
				else {
					printText += dialogue[i];
					if (dialogue[i] != ' ')
						yield return new WaitForSeconds(speed);
					textMesh.text = printText;
				}
			}
			yield return new WaitForSeconds(endPause);
		}
		Destroy (dialogueContainer);
		isActive = false;
	}

	GameObject drawDialogueContainer(){
		GameObject obj = Instantiate (containerPrefab, transform.position + defaultOffset , Quaternion.identity) as GameObject;
		obj.transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
		return obj;
	}
	
	void ControlCamera(){
		GetComponentInChildren<TempCameraTarget>().RunScript();
	}
	void ReturnCamera(){
		GetComponentInChildren<TempCameraTarget>().EndScript();
	}
}