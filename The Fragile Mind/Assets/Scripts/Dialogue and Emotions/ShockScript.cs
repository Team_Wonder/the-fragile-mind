using UnityEngine;
using System.Collections;

public class ShockScript : MonoBehaviour {

	// Use this for initialization
	void Start(){
		animation.Play();	
	}
	
	void Update(){
		transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
	}
	
	void Disable(){
		Destroy (transform.parent.gameObject);
	}
}
