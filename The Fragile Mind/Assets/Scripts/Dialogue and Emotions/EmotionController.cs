using UnityEngine;
using System.Collections;

public class EmotionController : MonoBehaviour {
	
	Texture[] browTex = new Texture[3];
	Texture[] eyeTex = new Texture[3];
	public Texture[] mouthTex = new Texture[3];
	Texture[] openMouthTex = new Texture[3];
	
	public float talkSpeed = 0.2f;
	
	public GameObject shockPrefab;
	public Vector3 shockOffset = Vector3.zero;
	
	public GameObject blushPrefab;
	public Transform facebone;
	
	private bool isTalking = false;
	private bool changeMouth = true;
	private int mouthIndex = 0;
	private bool mouthIsOpen = false;
	
	void Start(){
		//PlayEmotion("shock");
	}
	
	// Update is called once per frame
	void Update () {
		if (isTalking && changeMouth){
			changeMouth = false;
			talkMouth();
		}
	}
	
	public void updateBrows(string emo){
		if (emo == "normal")
			renderer.materials[1].mainTexture = browTex[0];
		else if (emo == "happy")
			renderer.materials[1].mainTexture = browTex[1];
		else if (emo == "sad")
			renderer.materials[1].mainTexture = browTex[2];
	}
	
	public void updateEyes(string emo){
		if (emo == "normal")
			renderer.materials[2].mainTexture = eyeTex[0];
		else if (emo == "happy")
			renderer.materials[2].mainTexture = eyeTex[1];
		else if (emo == "sad")
			renderer.materials[2].mainTexture = eyeTex[2];
	}
	
	public void updateMouth(string emo, bool isOpen){
		if (emo == "normal")
			mouthIndex = 1;
		else if (emo == "happy")
			mouthIndex = 2;
		else if (emo == "sad")
			mouthIndex = 3;
		if (isOpen)
			renderer.materials[3].mainTexture = openMouthTex[mouthIndex];
		else	
			renderer.materials[3].mainTexture = openMouthTex[mouthIndex];
		mouthIsOpen = isOpen;
	}
	
	IEnumerator talkMouth(){
		if (mouthIsOpen)
			renderer.materials[3].mainTexture = mouthTex[mouthIndex];
		else 
			renderer.materials[3].mainTexture = openMouthTex[mouthIndex];
		mouthIsOpen =! mouthIsOpen;
		yield return new WaitForSeconds(talkSpeed);
		changeMouth = true;
	}
	
	public IEnumerator talk(float time){
		isTalking = true;
		Debug.Log ("talk is being called???");
		yield return new WaitForSeconds(time);
		isTalking = false;
		//reset texture in case we ended with the wrong mouth
		if (mouthIsOpen)
			renderer.materials[3].mainTexture = openMouthTex[mouthIndex];
		else 
			renderer.materials[3].mainTexture = mouthTex[mouthIndex];
	}
	
	public void PlayEmotion(string emo){
		if (emo == "shock"){
			Transform obj = Instantiate (shockPrefab, transform.position + shockOffset , Quaternion.identity) as Transform;
			obj = obj.transform.Find("shock");
			//obj.transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
		}
		else if (emo == "blush"){
			GameObject obj = Instantiate (shockPrefab) as GameObject;
			obj.transform.parent = facebone;
			obj.transform.localPosition = Vector3.zero;
			obj.transform.localEulerAngles = Vector3.zero;
		}
	}
	
	public void Blush(bool state){
		transform.Find("blush").gameObject.SetActive(state);
	}
}
