using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {
	
	public Transform startTrans;
	public Transform endPos;
	public float totalTime = 5.0f;
	public float finishDistance = 0.0f;
	
	public bool easeStart = false;
	public bool easeEnd = false;
		
	public Transform target;
	public float damping = 0.0f;
	
	private float distance;
	protected float startTime;
	private Vector3 startPos;
	
	
	// Use this for initialization
	void Start () {
		if (startTrans == null)
			startPos = transform.position;
		else 
			startPos = startTrans.position;
		distance = Vector3.Distance(startPos, endPos.position) - finishDistance;
		startTime = Time.time;
	}
	// Update is called once per frame
	protected virtual void Update () {
		float ratio = (Time.time - startTime)/totalTime;
		if (ratio < 1){
			if (easeStart && ratio < 0.5)
				ratio = Mathf.SmoothStep(0, 1, (float)ratio);
			else if (easeEnd && ratio >= 0.5)
				ratio = Mathf.SmoothStep(0, 1, (float)ratio);
			//Debug.Log("ratio: "+ratio);
			transform.position = Vector3.MoveTowards(startPos, endPos.position, ratio*distance);
			
			if (target){
			// Look at and dampen the rotation
				var rotation = Quaternion.LookRotation(target.position - transform.position);
				transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 4);
			}
		}
	}
}
