using UnityEngine;
using System.Collections;

public class randomFloat : MonoBehaviour {
	
	private Vector3 origPosOffset;
	
	float speed = 0.45f;
	float positionOffset;
	
	// Use this for initialization
	void Start () {
		origPosOffset = transform.localPosition;
		positionOffset = Random.Range(-30, 30);
	}
	
	// Update is called once per frame
	void Update () {
		//Vector3 temp = origPosOffset;
		transform.localPosition = origPosOffset + new Vector3(0, Mathf.Sin(Time.time*speed + positionOffset)*0.5f,0);
	}
}
