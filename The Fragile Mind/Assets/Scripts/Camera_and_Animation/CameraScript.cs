using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public Transform target;
	public float minDistance = 2.0f;
	public float maxDistance = 10.0f;
	public float accel = 2f;
	public float height = 2.0f;
	
	public float damping = 0.5f;
	
	private Vector3 velocity = Vector3.zero;
	private Vector3 targetPos;
	private float tolerance = 1f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		
		var rotation = Quaternion.LookRotation(target.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
		
		
		targetPos = new Vector3(target.position.x, target.position.y + height, target.position.z);
		float dist = Vector3.Distance(targetPos, transform.position);
		if (dist > maxDistance){
		velocity += Vector3.Normalize(targetPos-transform.position)*accel*Time.deltaTime;
			//Debug.Log("moving forwards: "+velocity.magnitude/Time.deltaTime);*/
		}
		else if (dist < minDistance - tolerance){
			velocity -= Vector3.Normalize(targetPos-transform.position)*accel*Time.deltaTime;
			Debug.Log("moving backwards");
		}
		else velocity *= (0.95f);
		//Debug.Log("velocity "+velocity);
		
//		Debug.Log ("Vec: "+velocity);
		Vector3 temp = transform.position;
		transform.position = temp + velocity;
	}
}
