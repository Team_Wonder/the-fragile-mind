using UnityEngine;
using System.Collections;

public class TempCameraTarget : MonoBehaviour {
	
	public float minDist = 0.0f;
		
	public float timeOut = 0.0f;
	public bool usesMoveObjectScript = false;
	
	private float oldMinDist;
	
	void OnTriggerEnter(Collider col){
		if (col.tag == "Player")	
			RunScript ();
	}
	
	void OnTriggerExit(Collider col){
		if (col.tag == "Player")
			EndScript();
	}
		
	public void RunScript(){

		if (usesMoveObjectScript){
			Camera.main.GetComponent<CameraScript>().enabled =  false;
			MoveObject moT = Camera.main.gameObject.AddComponent("MoveObject") as MoveObject;
			MoveObject moF = GetComponent<MoveObject>();
			moT.target = moF.target;
			moT.damping = moF.damping;
			moT.startTrans = moF.startTrans;
			moT.endPos = moF.endPos;
			moT.totalTime = moF.totalTime;
			moT.finishDistance = moF.finishDistance;
			moT.easeStart = moF.easeStart;
			moT.easeEnd = moF.easeEnd;
		}
		else{
			Camera.main.GetComponent<CameraScript>().target = transform;
			if (minDist > 0.0f){
				oldMinDist = Camera.main.GetComponent<CameraScript>().minDistance;
				Camera.main.GetComponent<CameraScript>().minDistance = minDist;
			}
		}
		if (timeOut > 0.0f) 
			StartCoroutine(KillAfter(timeOut));
	}
		
	public void EndScript(){
		if (usesMoveObjectScript){
			Camera.main.GetComponent<CameraScript>().enabled = true;
			Destroy(Camera.main.GetComponent<MoveObject>());
		}
		Camera.main.GetComponent<CameraScript>().target = GameObject.FindGameObjectWithTag("Player").transform;
		if (minDist > 0.0f){
			Camera.main.GetComponent<CameraScript>().minDistance = oldMinDist;
		}

	}
	
	IEnumerator KillAfter(float t){
		yield return new WaitForSeconds(t);
		EndScript ();
		Destroy (this);
	}
}
