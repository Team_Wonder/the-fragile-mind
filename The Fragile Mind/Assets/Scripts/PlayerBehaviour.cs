using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
	public float minDistance = 3.0f;
	private GameObject[] interactables;
	private static GameObject nearestInteractable = null;
	
	// Use this for initialization
	void Start () {
		interactables = GameObject.FindGameObjectsWithTag("Interactable");
		nearestInteractable = GetNearestInteractable();
	}
	
	// Update is called once per frame
	void Update () {
		GameObject newNearest = GetNearestInteractable();

		if (newNearest != null) {
			if (newNearest != nearestInteractable){
				if (nearestInteractable != null)
					nearestInteractable.GetComponent<InteractiveObject>().DeselectObject();
				nearestInteractable = newNearest;
				nearestInteractable.GetComponent<InteractiveObject>().SelectObject();
			}
		}
		else if (nearestInteractable != null) {
			nearestInteractable.GetComponent<InteractiveObject>().DeselectObject();
			nearestInteractable = null;
		}
		
		if (Input.GetKeyDown("e")) {
			if (nearestInteractable){
				nearestInteractable.GetComponent<InteractiveObject>().UseObject();
			}
		}
	}
	
	// Finding the nearest interactable object to the player and highlighting it.
	GameObject GetNearestInteractable() {
		GameObject nearestInteractable = null;
		float distance = minDistance;
		Vector3 position = transform.position;
		
		foreach (GameObject obj in interactables) {
			Vector3 difference = (obj.transform.position - position);
			float curDistance = difference.sqrMagnitude;
			
			if (curDistance < distance) {
				nearestInteractable = obj;
				distance = curDistance;
			}
		}
		return nearestInteractable;
	}
}
