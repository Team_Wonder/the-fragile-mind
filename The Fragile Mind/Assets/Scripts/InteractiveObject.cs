using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour {
	public static float highlightOutline = 0.05f;
	public static float outline = 0.0f;
	public static Color highlightColour = Color.red;
	public static Color outlineColour = Color.black;
	public string[] specialScripts;
	
	private static GameObject player;
		
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	// Highlight the object
	public void SelectObject() {
		renderer.material.SetFloat("_Outline", highlightOutline);
		renderer.material.SetColor("_OutlineColor", highlightColour);
	}
	
	// Unhighlight the object
	public void DeselectObject() {
		renderer.material.SetFloat("_Outline", outline);
		renderer.material.SetColor("_OutlineColor", outlineColour);
	}
	
	// Interacting with the object
	public void UseObject() {
		Debug.Log ("The "+name+" was interacted with");
		foreach (string script in specialScripts)
			gameObject.SendMessage(script);
	}
}
