using UnityEngine;
using System.Collections;

public class ReturnScript : MonoBehaviour {
	
	bool menuOn = false;

	void Start(){
		Time.timeScale = 1.0f;	
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)){
			if (menuOn)
				CloseMenu();
			else 
				OpenMenu();
		}
	}

	
	void OnMouseDown(){
		CloseMenu ();
	}
	
	void OpenMenu(){
		Time.timeScale = 0.0f;
//		transform.parent.gameObject.active = true;
		menuOn = true;
	}
	
	void CloseMenu(){
		Time.timeScale = 1.0f;
//		transform.parent.gameObject.active = false;	
		menuOn = false;
	}
}
