# Game Studio 2 - Adventure Game Base Project #
Written By Benjamin Wallis

## Contact Information ##
**Lecturer** - Fabio Zambetta
[fabio.zambetta@rmit.edu.au](mailto:fabio.zambetta@rmit.edu.au)

**Head/Art/Design Tutor** - Cherie Davidson
[cherie.davison@rmit.edu.au](mailto:cherie.davidson@rmit.edu.au)

**Programming Tutor** - Benjamin Wallis
[benjamin.wallis@rmit.edu.au](mailto:benjamin.wallis@rmit.edu.au)