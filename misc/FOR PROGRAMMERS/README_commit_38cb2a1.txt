The New character experssion display:
  paragraphs now accept parameters beginning with the symbol '#'
  possible parameters are N = normal, P = pain, Y = yell, F = frightened S = smile
  E.G.
    paragraph[0] = ";#NHi, this is an normal experssion."
  
  Note:
    The parameter can ONLY be attached at the beginning of a text block, meaning always
    directly after the ';' symbol. The parameter characters themselves will also get 
    automaticly hidden on display, so dont worry about that;
    
    If there is no parameter, no experssion will be shown. Use this for narration 
    purposes.