HOW TO USE:
	Progression.cs:
		What it does:
			This is a script attached to a presistent object that tracks which scenes 
			the player has been to.
		Use:
			Currently (19/09) it is used for unique messages to be only displayed 
			once at the start of the scene (Done by GUITextDisplay.cs and 
			TextStorageA*R*.cs).
		How to:
			To add a scene to be tracked, add 
			[code] beenTo.Add("*SCENE ID*", "false"); [/code].
			Note: *SCENE ID* is the first 4 characters of the scene name
			E.g.
			[code] beenTo.Add("A3R2", "false"); [/code].
	
	TextStorageA*R*.cs (Current commit has 'TextStorageA1R1.cs'):
		What is does:
			This is a script attached to a unique object per scene and contains the
			displayable text (used in the text box) for that scene.
		Use:
			Currently (19/09) it is used for unique messages to be only displayed 
			once at the start of the scene (Done by passing the text block to 
			GUITextDisplay.cs).
		How to:
			Note: Make sure the class name is in the correct format with the last 
			four characters being the scene ID.
			Add text blocks (See instructions in method text()).
			Custom initializers for passing text blocks (See example in Update()).
		
		
WIP (Worth checking out):
	GUISceneChangeA1R1.cs:
		Just a simple GUI script to add buttons for scene change.
	
	GUITextDisplay.cs
		Checks alot of stuff, does alot of stuff, stuff.